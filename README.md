Assignment1 CS718
============

An assignment for PG cert in IT

Gitlab link
---

https://gitlab.com/doomium-chloride/assignment1-indus-tree

Git HTTPS clone link
---

https://gitlab.com/doomium-chloride/assignment1-indus-tree.git

Rules of the Bull and Cow game
============

There are 2 players, the user and the computer.

Each player gets to pick a secret code that the other must guess.

A code must have 4 unique digits from 0-9.

There may only be 7 rounds, if no winner is decided within 7 rounds the game is deemed to be a draw

Initial design
============

![initial UML design](Assignment/UML_initial.png)

The original idea was to create a class called Code with all the necessary
tools to play the game, such as it cat check itself if it's valid or not,
calculate Bulls and Cows.
Since we need to be able to choose different computer strategies, we need a
parent class all AI difficulty to be a descendent of. I made it an abstract 
class called GameAI. The reason for it being an abstract class instead of 
an interface is, mainly why not. But also so I can implement some methods or 
instance variables inside the abstract class.

I originally thought that there was only 1 secret code that both the player 
and the computer had to guess first. So there are some artifacts from 
that initial misunderstanding.

Final design
============

The Code class is now omnipresent, so I will ignore it's association and dependency 
relationships because it will get too convoluted. Just like how we don't 
show the String class or ArrayList class in a normal UML.

Also the StartGame class and Exception classes were omitted.

![Final UML design](Assignment/UML_final.png)

The game pretty much follows the initial design with an additional 
GameRecorder object to record every round. I initially made GameRecorder 
record every guess but I thought that every round would be better, 
so I just encapsulated the guesses into a Round object. Originally each round 
had information on which turn it is, but I removed it and put it in the 
GameRecorder.

I also added a Utility class for cross-class reusable code.

Why I think this design is good for this assignment
============

I think this also counts as part of the reflection.

This assignment requires us to use inheritance, and this design uses a lot 
of inheritance. Mainly inheriting RunTimeExceptions.
This design is also very object oriented as it heavily relies on Exceptions 
to make decision.

Why I think this design is bad
============

Exceptions, it's very reliant on it. Which may be good or bad.

Too many cheats. I put too many cheats into it.

Not resilient to bad file format. We assumed that the user will give the game 
a correctly formatted guess sheet. This might not be the case.

One file per saved game. It would be nice to be able to append a saved game 
to an existing save. But I didn't implement that. Mostly because the assignment 
didn't require me to.

Reflection
============

Since start of this paper
---
This paper has taught me object oriented programming and UML diagrams.
My prior experience was mostly python in physics, so essentially mathematical 
programming. So it was more functional even though python is object oriented.

I've been able to do all the labs.
I learnt how to make use of an IDE.
Now I'm rather dependent on using an IDE since I still don't know how to compile 
an entire project using only the terminal and a text editor.

I've learnt how to use git a little bit.
Also I copied the way the labs wrote markdown files for the readme.
Plus the stuff from 719.

UML class diagrams
---

I learnt that UML diagrams are kind of annoying to do.
I should figure out a way to automate it.
I guess IntelliJ has one way but apparently it's not detailed enough.

RuntimeExceptions
---

RuntimeExceptions are awesome!
You can do many things with them, and drastically alter the flow of your 
program. This is also a possible way to get around the restriction of 
a method only being able to return one type of object or primitive data.
You can throw exceptions instead! But this will probably be deemed bad 
design if I abuse it and I think exceptions aren't the most efficient way 
to control flow. But they're a nice way sometimes.