package main;

import java.util.Map;
import java.util.TreeMap;
/*
This allows for the game to be recorded then written to a savefile
 */
public class GameRecorder {
    private Code botCode, playerCode;
    private Map<Integer,Round> rounds;
    private String winner;
    /*
    set up and setting default winner message.
     */
    public GameRecorder(Code botCode, Code playerCode){
        this.botCode = botCode;
        this.playerCode = playerCode;
        winner = "no winner?";
        rounds = new TreeMap<>();
    }
    /*
    adds a guess to the treemap
     */
    public void addGuess(int turn, boolean isPlayer, Code code, int[] cb){
        Round bout = rounds.get(turn);
        if (bout == null){
            bout = new Round();
            rounds.put(turn,bout);
        } else {
            bout = rounds.get(turn);
        }
        if (isPlayer){
            bout.player = new Guess(code,cb);
        } else {
            bout.bot = new Guess(code,cb);
        }
    }
    /*
    winner is private so this is a setter
     */
    public void setWinner(String winner){
        this.winner = winner;
    }
    /*
    helps with string making logic
     */
    private String makeHeader(){
        return "Your code: " + playerCode.toString() + "\nComputer's code: " + botCode.toString();
    }
    /*
    makes a string of one round
     */
    private String printRound(Round round, int turn){
        String str = "\n---\nTurn " + turn + ":\n";
        str += "You guessed " + round.player.code + ", scoring " + round.player.printCB();
        str += "\nComputer guessed " + round.bot.code + ", scoring " + round.bot.printCB();
        return str;
    }
    /*
    returns a string to be saved into the savefile
     */
    @Override
    public String toString() {
        String str = "Bulls and Cows game result.\n";
        str += makeHeader();
        for (int turn : rounds.keySet()){
            str += printRound(rounds.get(turn),turn);
        }
        return str + "\n---\n" + winner;
    }
}
