package main;

import main.Exceptions.Code.PossibleCodeGenerationException;
/*
A bunch of functions multiple classes might use
 */
public class Utility{
    /*
    returns a random int digit 0-9
     */
    public static int randomDigit(){
        return randRange(0,10);
    }
    /*
    randRange takes 2 parameters and returns a random integer between a and b not including b, including a.
     */
    public static int randRange(int a, int b){
        int range = b - a;
        return (int) (Math.random()*range) + a;
    }
    /*
    Prints a prompt and gets what user types into console
     */
    public static String input(String prompt){
        System.out.print(prompt);
        return Keyboard.readInput();
    }
    /*
    converts an integer to a 4 char long string
     */
    public static String int2str(int num){
        String str = "" + num;
        if (str.length() > 4){
            throw new PossibleCodeGenerationException();
        }
        while (str.length() < 4){
            str = "0" + str;
        }
        return str;
    }
    /*
    checks if 2 int arrays are the same
     */
    public static boolean same(int[] a, int[] b){
        if (a.length != b.length){return false;}
        for (int i = 0; i < a.length; i++) {
            if (a[i] != b[i]){return false;}
        }
        return true;
    }
    /*
    like input, but for yes no questions
     */
    public static boolean yesNo(String prompt){
        String choice = input(prompt).toLowerCase();
        return (choice.length() != 0 && choice.charAt(0) == 'y');
    }
    /*
    prints cows, bulls or both.
    takes into account singular or plural.
     */
    public static String printBulls(int[] cb){
        if (cb[1] == 1){
            return "1 bull";
        }
        return cb[1] + " bulls";
    }
    public static String printCows(int[] cb){
        if (cb[0] == 1){
            return "1 cow";
        }
        return cb[0] + " cows";
    }
    public static String printBullsAndCows(int[] cb){
        return printBulls(cb) + " and " + printCows(cb);
    }
}