package main.game;

import main.AI.EasyAI;
import main.AI.GameAI;
import main.AI.HardAI;
import main.AI.MediumAI;
import main.Code;
import main.Exceptions.Cheats.BotCheat;
import main.Exceptions.Cheats.CheatException;
import main.Exceptions.Cheats.DebugCheat;
import main.Exceptions.Cheats.WinCheat;
import main.Exceptions.Code.CodeException;
import main.Exceptions.GamePlay.QuitException;
import main.Exceptions.GamePlay.TieException;
import main.GameRecorder;
import main.Utility;

import java.io.*;
import java.util.Scanner;

class Game {
    private HardAI playerSlave;//cheating :P
    private GameAI bot;//computer
    private Code secret;//computer's secret code
    private Code playerCode;//player's secret code
    private String winner;//message when someone wins or game ends
    final private String originalMessage = "You win! ☺";
    private String playerWinMessage = originalMessage;
    private FileReader file;//file of prescribed guesses
    private Scanner scanner;//help read the prescribed guesses
    private int turn;//what turn it is. starts at 1
    private GameRecorder recorder;//to record every turn
    /*
    listens for  QuitException to allow the game to end early
     */
    void moo(){
        try{
            do{
                start();
                bot = null;
            }
            while (Utility.yesNo("Play again? (y/n) "));
        } catch (QuitException quit){
            System.out.println("\nGame will terminate");
        }
        System.out.println("\nBye bye");
    }
    /*
    Sets up the secret codes and player method, use a file or not to use a file
     */
    private void setUp(){
        playerSlave = new HardAI();
        turn  = 1; winner = "Something's not right...";
        showMenu();//choose between manual or automatic
        System.out.println("---");
        playerCode = getPlayerCode("Please enter your secret code: ", false);
        secret = new Code();
        recorder = new GameRecorder(secret, playerCode);
        System.out.println("---");
    }
    /*
    Allows for selection of AI difficulty and keeps the game going until someone wins or there is a tie.
     */
    private void start(){
        setUp();
        while (bot == null){
            bot = chooseAI();
        }
        if (file != null){
            scanner = new Scanner(file);
        }
        try {
            while (noWinner()) {
                if (turn >= 7){//If there's a winner this will not execute
                    throw new TieException();
                }
                turn++;
            }
        } catch (TieException tie){
            winner = "It's a tie";
        }
        printWinner();
        String option = Utility.input("Would you like to save the game? (y/n) ").toLowerCase();
        if (option.length() == 0){return;}
        if (option.charAt(0) == 'y'){
            saveGame();
        }
    }
    /*
    Allows for a completed game to be saved
     */
    private void saveGame(){
        String fileName = Utility.input("Please enter fileName: ");
        try {
            if (fileExists(fileName)){
                String overwrite = Utility.input("File already exists, overwrite? (y/n) ").toLowerCase();
                if (overwrite.length() != 0 && overwrite.charAt(0) == 'y'){
                    write2file(fileName);
                } else {
                    if (Utility.yesNo("Choose another file to save to? (y/n) ")){
                        saveGame();
                    }
                }
            } else {
                write2file(fileName);
            }
        } catch (IOException e){
            if (Utility.yesNo("An error occurred. Try again? (y/n) ")){
                saveGame();
            }
        }
    }
    /*
    writes the recorded game to the save file
     */
    private void write2file(String fileName) throws IOException{
        try(BufferedWriter printer = new BufferedWriter(new FileWriter(fileName, false))) {
            printer.write(recorder.toString());
            System.out.println("Game saved");
        }
    }
    private boolean fileExists(String file_name){//checks if a file already exists
        File fileCheck = new File(file_name);
        return fileCheck.isFile();
    }
    private void welcome(){
        System.out.println("Welcome to the Cow and Bull game\n---\n");
    }
    /*
    guess method selection
     */
    private void showMenu(){
        welcome();
        System.out.println("How would you like to guess?");
        System.out.println("Manually, or using a file containing the guesses?");
        while (true) {
            System.out.println("For manual enter \"m\", if using a file enter \"f\"");
            String userInput = Utility.input("Enter choice: ").toLowerCase();
            if (userInput.length() == 0){
                System.out.println("Invalid input");
            } else if (userInput.charAt(0) == 'm'){
                file = null;
                return;
            } else if (userInput.charAt(0) == 'f'){
                String fileName = Utility.input("Enter file name: ");
                try{
                    file = new FileReader(fileName);
                    return;
                } catch (FileNotFoundException noFile){
                    System.out.println("no such file");
                }
            }
        }

    }
    /*
    AI difficulty selection
     */
    private GameAI chooseAI(){
        String choice = Utility.input("Choose difficulty.\neasy\nmedium\nhard\n---\n");
        switch (choice.toLowerCase()){
            case "easy":
            case "e":
                System.out.println("Easy AI chosen");
                return new EasyAI();
            case "medium":
            case "m":
            case "med":
                System.out.println("Medium AI chosen");
                return new MediumAI();
            case "hard":
            case "h":
                System.out.println("Hard AI chosen");
                return new HardAI();
        }
        System.out.println("Invalid Choice!");
        return null;
    }
    /*
    To make the logic more clear in the start method, while(noWinner) keep the game going
     */
    private boolean noWinner(){
        System.out.println("---");
        try {
            return checkFile();
        } catch (DebugCheat debug){
            System.out.println("Player code = " + playerCode);
            System.out.println("Bot code = " + secret);
        } catch (CheatException e){
            System.out.println("Unsupported cheat entered!");
        }
        turn--;
        return true;
    }
    /*
    check if you're reading off a file to make your guesses
    if so, then it reads of the next line and makes it a code object. if no more lines it gets rid of the scanner
    If not reading of file, it records the playerGuess.
     */
    private boolean checkFile(){
        Code playerGuess;
        if (scanner == null) {
            try {
                playerGuess = getPlayerGuess();
            } catch (BotCheat cheat1) {
                playerGuess = playerSlave.guessCode();
                System.out.println("The guess is: " + playerGuess);
            } catch (WinCheat win) {
                playerWinMessage = win.narrate();
                playerGuess = secret;
            }
        } else {
            if (scanner.hasNextLine()){
                playerGuess = new Code(scanner.nextLine());
                System.out.println("Your guess: " + playerGuess.toString());
            } else{
                scanner = null;
                return checkFile();
            }
        }
        return oneRound(playerGuess);
    }
    /*
    one round of the game.
    player guesses once, bot guesses once. then checks for winners so both bot and player have same number of chances
    Ties are possible
     */
    private boolean oneRound(Code playerGuess){
        int[] playerCB = showCowsAndBulls(playerGuess, secret);
        playerSlave.seePlayerGuess(playerGuess,playerCB);
        recorder.addGuess(turn, true, playerGuess, playerCB);
        boolean oneWinner = false;
        if (check(playerGuess, secret)){
            winner = playerWinMessage;
            oneWinner = true;}
        Code AIguess = getAIGuess();
        int[] AICB = showCowsAndBulls(AIguess, playerCode);
        recorder.addGuess(turn, false, AIguess, AICB);
        bot.seeResultOfLastGuess(AICB);
        if (check(AIguess, playerCode)){
            winner = bot.victory();
            if (oneWinner){
                throw new TieException();// if both player and computer wins, it's a tie.
            } else {
                oneWinner = true;
            }
        }
        if (oneWinner){return false;}// one player has won
        System.out.println("---");
        return true;// no one has won
    }
    /*
    gets player's guess
     */
    private Code getPlayerGuess(){
        return getPlayerCode("Your guess: ", true);
    }
    /*
    gets a code from the player
    Can exit game early
     */
    private Code getPlayerCode(String prompt, boolean cheatOn){
        while(true) {
            try {
                String guess = Utility.input(prompt);
                if (guess.toLowerCase().equals("exit") || guess.toLowerCase().equals("quit")){
                    throw new QuitException();
                }
                if (cheatOn) {
                    checkCheat(guess);
                }
                return new Code(guess);
            } catch (CodeException e){
                System.out.println(e.getMessage());
            }
        }
    }
    /*
    gets the AI to guess a code
     */
    private Code getAIGuess(){
        Code botGuess =  bot.guessCode();
        System.out.println("Computer guess: " + botGuess.toString());
        return botGuess;
    }
    /*
    check if 2 codes are the same
     */
    private boolean check(Code guess, Code target){
        return guess.equals(target);
    }
    /*
    checks the cows and bulls between 2 codes.
    prints the cows and bulls, then
    returns an int array with format {cows, bulls}
     */
    private int[] showCowsAndBulls(Code guess, Code target){
        int[] cb = Code.cb(guess,target);
        System.out.println("Result: " + Utility.printBullsAndCows(cb));
        return cb;
    }
    /*
    prints the victory message of the winner
     */
    private void printWinner(){
        System.out.println();
        System.out.println(winner);
        recorder.setWinner(winner);
        playerWinMessage = originalMessage;
    }
    /*
    allow cheats. this is the cheat list.
    Uses CheatException Objects to activate the cheats
    Used for exceptional flow control
     */
    private void checkCheat(String cheatCode){
        String cheat = cheatCode.toLowerCase();
        switch (cheat){
            case "doom":
            case "explosion":
            case "burn":
            case "win":
                throw new WinCheat(cheat);
            case "hax":
            case "cheat":
            case "help":
                throw new BotCheat();
            case "debug":
                throw new DebugCheat();
        }
    }
}
