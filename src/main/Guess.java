package main;
/*
To help with recording the game
 */
public class Guess {
    private int[] cb;
    public Code code;
    Guess(Code code, int[] cb){
        this.code = code;
        this.cb = cb;
    }
    /*
    returns string of show amount of bulls and cows
     */
    String printCB(){
        return Utility.printBullsAndCows(cb);
    }
}
