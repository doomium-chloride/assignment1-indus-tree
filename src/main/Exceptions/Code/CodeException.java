package main.Exceptions.Code;

public class CodeException extends RuntimeException{
    @Override
    public String getMessage() {
        return "Invalid Code";
    }
}
