package main.Exceptions.Code;


public class CodeContentException extends CodeException {
    @Override
    public String getMessage() {
        return "Each digit in the code must be a unique integer from 0-9";
    }
}
