package main.Exceptions.Code;

public class PossibleCodeGenerationException extends RuntimeException{
    @Override
    public String getMessage() {
        return "Something unexpected happen during the generation of possible codes";
    }
}
