package main.Exceptions.Code;

public class CodeLengthException extends CodeException {
    int digits = 4;
    @Override
    public String getMessage() {
        return "Code must be " + digits +  " digits";
    }
}
