package main.Exceptions.GamePlay;

public class TieException extends GameControlException{
    @Override
    public String getMessage() {
        return "No one won.\nAll has been lost.";
    }
}
