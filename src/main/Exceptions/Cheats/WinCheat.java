package main.Exceptions.Cheats;
/*
Cheat by winning the game
 */
public class WinCheat extends CheatException{
    private String aftermath;
    private String def = "All hail your greatness";//default
    public WinCheat(){
        aftermath = def;
    }
    public WinCheat(String method){
        switch (method){
            case "doom":
                aftermath = "You were doomed to win anyway"; break;
            case "win":
                aftermath = "Fine... you win boss"; break;
            case "burn":
                aftermath = "Opponent eliminated. You win."; break;
            case "explosion":
                aftermath = "黒より黒く闇より暗き漆黒に我が深紅の混淆を望みたもう。\n覚醒のとき来たれり。\n無謬の境界に落ちし理。\n無行の歪みとなりて現出せよ！\n踊れ踊れ踊れ、我が力の奔流に望むは崩壊なり。\n並ぶ者なき崩壊なり。\n万象等しく灰塵に帰し、深淵より来たれ！\nこれが人類最大の威力の攻撃手段、これこそが究極の攻撃魔法、エクスプロージョン！";
                break;
            default:
                aftermath = def;
        }
    }
    public String narrate(){
        return aftermath;
    }
}
