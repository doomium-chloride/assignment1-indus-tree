package main;

import main.Exceptions.Code.CodeContentException;
import main.Exceptions.Code.CodeLengthException;

public class Code{
    /*
    The code must have 4 unique digits. It can check if it's valid or not.
     */
    final public static int length = 4;
    private int[] digits = new int[length];
    public Code(){
        do{
            for (int i = 0; i < length; i++) {
                digits[i] = Utility.randomDigit();
            }
        }
        while (!isValid(digits));
    }
    public Code(int[] guess){
        if (guess.length != 4){
            throw new CodeLengthException();
        }
        if (isValid(guess)) {
            digits = guess;
        }else{
            throw new CodeContentException();
        }
    }
    public Code(int guess){
        this(int2array(guess));
    }
    public Code(String guess){
        this(str2array(guess));
    }
    /*
    converts an integer or string into an array of integers
    only for the constructor
     */
    private static int[] int2array(int guess){
        if (guess >= 10000){
            throw new CodeLengthException();
        }
        String nums = "" + guess;
        return str2array(nums);
    }
    private static int[] str2array(String guess){
        if (guess.length() > 4 || guess.length() < 4){
            throw new CodeLengthException();
        }
        int[] digit = new int[length];
        for (int i = 0; i < length; i++) {
            digit[i] = guess.charAt(i) - 48;
        }
        return digit;
    }
    /*
    checks if a given code is valid or not
     */
    private boolean isValid(int[] code){
        for (int i = 0; i < code.length; i++) {
            for (int j = 0; j < i; j++) {
                if (code[i] == code[j]){
                    return false;
                }
            }
        }
        for (int d : code){
            if (d > 9 || d < 0){
                return false;
            }
        }
        return true;
    }
    /*
    bulls is number of correct numbers in correct position
     */
    public int getBulls(Code other){
        int count = 0;
        for (int i = 0; i < length; i++) {
            if (this.digits[i] == other.digits[i]){
                count++;
            }
        }
        return count;
    }
    /*
    cows is number of correct numbers in wrong position;
     */
    public int getCows(Code other){
        int count = 0;
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++) {
                if (this.digits[i] == other.digits[j] && i != j){
                    count++;
                }
            }
        }
        return count;
    }
    /*
    2 codes are equal when all digits are equal and in correct order
     */
    @Override
    public boolean equals(Object obj) {
        Code o;
        if (obj instanceof Code){
            o = (Code) obj;
        } else{
            return false;
        }
        for (int i = 0; i < length; i++) {
            if (this.digits[i] != o.digits[i]){
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString() {
        String str = "";
        for (int i = 0; i < length; i++) {
            str += digits[i];
        }
        return str;
    }
    /*
    shortcut for convenience.
    returns {cows, bulls} int[]
     */
    public static int[] cb(Code a, Code b){
        return new int[]{a.getCows(b),a.getBulls(b)};
    }
}