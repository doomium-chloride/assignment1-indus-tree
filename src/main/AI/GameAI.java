package main.AI;
import main.Code;
public abstract class GameAI{
    static int maxGuess = 3024;//9p4
    /*
    The AI attempts to guess the secret code
     */
    public abstract Code guessCode();
    //public abstract void seePlayerGuess(Code guess, int[] cb); // useless
    /*
    AI gets to see what cows and bulls it's last guess has
     */
    public abstract void seeResultOfLastGuess(int[] cb);
    /*
    AI boasts it's superiority
     */
    public String victory(){
        return "AI superiority is proven";
    }
}