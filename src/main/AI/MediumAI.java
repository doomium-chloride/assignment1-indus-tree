package main.AI;

import main.Code;

import java.util.HashSet;
import java.util.Set;

public class MediumAI extends GameAI{
    private static int maxGuess = GameAI.maxGuess;
    private Set<Code> guessed;
    public MediumAI(){
        guessed = new HashSet<>(maxGuess);
    }
    @Override
    public Code guessCode() {
        Code test;
        if (guessed.size() >= maxGuess){
            return new Code();
        }
        while (true){
            test = new Code();
            if (! guessed.contains(test)){
                guessed.add(test);
                return test;
            }
        }
    }
    /*
    When creating a guessing a code. it not only makes a new random valid code, it also checks if it has guessed it or not.
     */

    @Override
    public void seeResultOfLastGuess(int[] cb) {
        // Do nothing
    }
}
