package main.AI;

import main.Code;
import main.Exceptions.Code.CodeException;
import main.Utility;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class HardAI extends GameAI{
    static private int codeLen = Code.length;
    private Code lastGuess;
    private Set<Code> possibleCodes;
    private Iterator<Code> codeGen;
    public HardAI(){
        int tries = (int) Math.round(Math.pow(10,codeLen));
        possibleCodes = new HashSet<>(GameAI.maxGuess);
        generatePossibleCodes(tries);
        restoreCodeGen();
    }
    @Override
    public Code guessCode() {
        Code temp = codeGen.next();
        codeGen.remove();
        lastGuess = temp;
        return temp;
    }
    /*
    This checks and removes any codes that will not match the secret code
     */
    @Override
    public void seeResultOfLastGuess(int[] cb) {
        while (codeGen.hasNext()){
            int[] nextCB = Code.cb(codeGen.next(),lastGuess);
            if (! Utility.same(nextCB,cb)){
                codeGen.remove();
            }
        }
        restoreCodeGen();
    }

    public void seePlayerGuess(Code guess, int[] cb) { //useless since I didn't misunderstood the rules
        lastGuess = guess;
        seeResultOfLastGuess(cb);
    }
    /*
    This initially makes a set of all possible codes
     */
    private void generatePossibleCodes(int tries){
        for (int i = 0; i < tries; i++) {
            try{
                possibleCodes.add(new Code(Utility.int2str(i)));
            } catch (CodeException e){
                //there's nothing to do just skip.
            }
        }
    }
    /*
    resets the iterator. Just so I can have one line without an assignment to make the logic nicer
     */
    private void restoreCodeGen(){
        codeGen = possibleCodes.iterator();
    }
}
